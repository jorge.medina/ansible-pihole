#!/bin/bash

#
# script: generate-ssh-keys.sh
# author: Jorge Armando Medina
# desc: Generate SSH RSA 4096 keys for ansible.

# Enable debug mode and log to file
export DEBUG=1
LOG=generate-ssh-keys.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

# vars

# main

# Create ssh directory
pwd
sudo mkdir -p /etc/ansible/inventory/.ssh
sudo chmod 700 /etc/ansible/inventory/.ssh
cd /etc/ansible/inventory/.ssh

echo "Generating RSA/4096 SSH keys for ansible user."
sudo ssh-keygen -t rsa -b 4096 -C "ansible@home" -f id-ansible.rsa -q -N ""

echo "Changing key permissons to 600."
sudo chmod 600 *
