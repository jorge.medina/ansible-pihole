# ansible-pihole

## Introducción

En este respositorio tenemos los playbooks de ansible para automatizar la instalación
y configuración de un servidor Raspberry Pi OS (buster 10) que funcionará como servidor
DHCP y DNS usando el grandioso software pi-hole.

## Requisitos

Necesitamos dos máquinas, una desde donde usaremos ansible, diremos que es el nodo controlador,
y otra máquina que será la que controlaremos.

Ambas máquinas deben de tener conectividad directa para el protocolo SSH.

## Instalación y configuración

Debes instalar ansible localmente:

```
$ bin/bootstrap.sh
```

El script anterior instala ansible y deja listo todo en `/etc/ansible`.

Verifica que ansible está instalado:

```
$ ansible --version
```

Ahora debes generar tus llaves RSA usando el script:

```
$ cd /etc/ansible
$ bin/general-ssh-keys.sh
```

Este script genera un par de llaves en `inventory/.ssh`.

Ahora debes copiar tu llave publica a los nodos a administrar:

```
$ ssh-copy-id -i /etc/ansible/inventory/.ssh/id-ansible.rsa.pub pi@192.168.233.10
```

El comando anterior copiara la llave pública al archivo `/home/pi/.ssh/authorized_keys` en la
máquina 192.168.233.10.

Ahora debes modificar tu inventario para definir los parámetros de configuración correctamente:

```
$ vim inventory/hosts
```

Ahora que ya tienes configurado el inventario, puedes probar la conexión:

```
$ ansible rpi -m ping
```

Listo ya tienes ansible funcionando.

## Pruebas y Ejecución de playbook

Antes de ejecutar el playbook debemos probar que la sintaxis está correcta:

```
$ ansible-playbook --syntax-check deploy.yml
```

En caso de que no aparezca ningún error, ya se puede ejecutar el playbook:

```
$ ansible-playbook deploy.yml
```


